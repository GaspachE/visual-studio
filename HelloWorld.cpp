﻿#include <iostream>

// Функия вывода сообщения в консоль
void print()
{
    std::cout << "Hello SkillBox!\n";
}


int main()
{
    std::cout << "Hello World!\n";

    int x = 100;
    int y = 200;

    // Вывод изначально заданных значений
    std::cout << "X = " << x << "\n" << "Y = " << y << std::endl;

    // Программа обмена значаениями двух переменных
    int z = x;
    x = y;
    y = z;

    // Вывод конечных значений 
    std::cout << "X = "<< x << "\n" << "Y = " << y;
}